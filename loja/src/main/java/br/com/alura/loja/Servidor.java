package br.com.alura.loja;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {
	
	public static void main(String[] args) throws IOException {
		
		HttpServer server = inicializaServidor();
		System.out.println("Servidor Rodando");
		System.in.read();
		server.stop();
		
	}

	public static HttpServer inicializaServidor() {
		ResourceConfig config = new ResourceConfig().packages("br.com.alura.loja.resource");//adiciona o pacote
		URI uri = URI.create("http://127.0.0.1:8080/");//cria a uri
		return GrizzlyHttpServerFactory.createHttpServer(uri, config);//cria o servidor
	}

}
