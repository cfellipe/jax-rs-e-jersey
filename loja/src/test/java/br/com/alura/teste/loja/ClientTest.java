package br.com.alura.teste.loja;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.alura.loja.Servidor;
import br.com.alura.loja.modelo.Carrinho;

public class ClientTest {
	
	HttpServer server;
	
	@Before
	public void startaServidor() {
		 server = Servidor.inicializaServidor();
	}
	
	@After
	public void encerraServidor() {
		server.stop();
	}
	
	@Test
	public void testaConexaoComOServidor() {
		
		Client client = ClientBuilder.newClient();//criando o client
		WebTarget target = client.target("http://localhost:8080/");
		Carrinho carrinho = target.path("/carrinhos/1").request().get(Carrinho.class);
		Assert.assertEquals("Videogame 4",carrinho.getProdutos().get(0).getNome());
		System.out.println(carrinho);
		
	}

}
